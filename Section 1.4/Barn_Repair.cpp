/*
ID: xxxxxxxx
PROG: barn1
LANG: C++11
*/
#include <fstream>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <cstring>
#include <vector>
#include <stack>
#include <queue>
using namespace std;
ofstream fout ("barn1.out");
ifstream fin ("barn1.in");

int M, C, S;
vector<int> a;
vector<int> b;
int stalls[201];
bool greaterF(int a, int b){
    return a > b;
}       
int main()
{
    memset(stalls, 0, 201*sizeof(int));
    fin >> M >> S >> C;
    for(int i = 0; i < C; ++i){
        int stallNumber;
        fin >> stallNumber;
        stalls[stallNumber] = 1;
        a.push_back(stallNumber);
    }

    sort(a.begin(), a.end());

    int length = 0;
    int i = a[0];
    while(i <= a[C-1]){
        if(stalls[i] == 0) ++length;
        else if(length != 0){
            b.push_back(length);
            length = 0;
        }
        ++i;
    }
    for(int i = 0; i < b.size(); ++i)
    {
        //cout<<"length "<<emptySpaces[i]<<endl;
    }
    sort(b.begin(), b.end(), greaterF);
    int addedBarns = 0;
    for(int i = 0; i < b.size(); ++i)
    {
        if(i+1 <= M - 1) continue;
        addedBarns += b[i];
    }
    fout << C + addedBarns << endl;
    return 0;
}